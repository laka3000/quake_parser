# Quake log parser

## Setup

`bundle install`

## Run script

`~$ bin/quake_parser`

or specify the log file path as an argument

`~$ bin/quake_parser path/to/log_file.log`

## Test

`~$ rspec`

