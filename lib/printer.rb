class Printer
  class<<self
    def print(json_games, ranking)
      @json_games = json_games
      @ranking = ranking
      puts compose_text
    end

    private

    def compose_text
      line_break = '-' * 20
      "Games:\n#{@json_games}\n#{line_break}"\
      "\n"\
      "Player ranking:\n\n#{@ranking}\n"
    end
  end
end
