require 'game_log_slicer'
require 'game_parser'
require 'games_to_json_parser'
require 'ranking'
require 'printer'

class QuakeParser
  class << self
    def parse(path = './games.log')
      game_logs = get_logs_from_path(path)
      games = get_games_from_logs(game_logs)
      ranking = get_ranking_from_games(games)
      json_games = jsonfy_games(games)
      print_to_screen(json_games, ranking)
    end

    private

    def get_logs_from_path(path)
      GameLogSlicer.slice_games_from_log(path)
    end

    def get_games_from_logs(game_logs)
      game_logs.map { |game_log| GameParser.extract_game_from_log_slice(game_log) }
    end

    def get_ranking_from_games(games)
      Ranking.rank_players_from_games(games)
    end

    def jsonfy_games(games)
      GamesToJsonParser.parse_games_to_json(games)
    end

    def print_to_screen(json_games, ranking)
      Printer.print(json_games, ranking)
    end
  end
end
