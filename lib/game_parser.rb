require 'game'
require 'player_parser'
require 'total_kill_parser'

class GameParser
  class << self
    def extract_game_from_log_slice(game_log)
      @log = game_log
      Game.new(total_kills, players)
    end

    private

    def total_kills
      TotalKillParser.extract_total_kills_from_log(@log)
    end

    def players
      PlayerParser.extract_players_from_log(@log)
    end
  end
end
