module TextSelector
  def select_text_from_log(log, text)
    log.select { |line| line.include?(text) }
  end
end
