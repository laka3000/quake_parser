class Game
  attr_reader :total_kills
  attr_reader :players

  def initialize(total_kills, players)
    @total_kills = total_kills
    @players = players
  end
end
