require 'json'

class GamesToJsonParser
  class << self
    def parse_games_to_json(games)
      @games = games
      JSON.pretty_generate(hash_of_all_games)
    end

    private

    def hash_of_all_games
      @games.each_with_index.map do |game, index|
        ["game_#{index + 1}", hashify_game(game)]
      end.to_h
    end

    def hashify_game(game)
      total_kills = game.total_kills
      players_array = arrayfy_players(game.players)
      kills_hash = hashify_player_kills(game.players)
      { :total_kills => total_kills, :players => players_array, :kills => kills_hash }
    end

    def arrayfy_players(players)
      players.map(&:nick)
    end

    def hashify_player_kills(players)
      players.map { |player| [player.nick, player.kills] }.to_h
    end
  end
end
