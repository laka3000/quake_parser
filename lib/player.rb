class Player
  attr_reader :nick
  attr_accessor :kills

  def initialize(nick, kills = 0)
    @nick = nick
    @kills = kills
  end
end
