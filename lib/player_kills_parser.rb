require 'player'
require 'text_selector'

class PlayerKillsParser
  extend TextSelector

  class << self
    def update_player_kills_from_log(game_log, players)
      @kills = select_kills_from_log(game_log)
      players.each { |player| count_kills(player) }
      players.each { |player| discount_world_kills(player) }
    end

    private

    def discount_world_kills(player)
      player.kills -= player_world_kills(player)
    end

    def count_kills(player)
      player.kills += player_kills(player)
    end

    def player_world_kills(player)
      @kills.select do |line|
        killed = killed_by_world_expression.match(line)
        !killed.nil? && killed[:nick] == player.nick
      end.length
    end

    def player_kills(player)
      @kills.select { |line| player_killed_expression.match(line)[:nick] == player.nick }.length
    end

    def select_kills_from_log(game_log)
      select_text_from_log(game_log, 'Kill: ')
    end

    def killed_by_world_expression
      /Kill:\s1022\s\d+\s\d+:\s<world>\skilled\s(?<nick>.+)\sby/
    end

    def player_killed_expression
      /Kill:\s\d+\s\d+\s\d+:\s(?<nick>.+)\skilled/
    end
  end
end
