class GameLogSlicer
  class << self
    def slice_games_from_log(path)
      @log = read_log_file_from_path(path)
      set_game_init_indexes
      slice_each_game_log
    end

    private

    def read_log_file_from_path(path)
      File.open(path).read.split("\n")
    end

    def slice_each_game_log
      @game_init_indexes.map.with_index do |game_init_position, index|
        slice_game(game_init_position, index)
      end
    end

    def set_game_init_indexes
      game_init_text = 'InitGame:'
      @game_init_indexes = []
      @log.select.with_index { |line, index| @game_init_indexes << index if line.include?(game_init_text) }
    end

    def slice_game(game_init_position, index)
      next_game_init_index = @game_init_indexes[index + 1]
      current_game_end = next_game_init_index.nil? ? @log.length - 1 : next_game_init_index
      @log[game_init_position..current_game_end]
    end
  end
end
