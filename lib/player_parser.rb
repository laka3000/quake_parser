require 'player'
require 'text_selector'
require 'player_kills_parser'

class PlayerParser
  extend TextSelector

  class << self
    def extract_players_from_log(game_log)
      players = create_player_objects_from_log(game_log)
      PlayerKillsParser.update_player_kills_from_log(game_log, players)
    end

    private

    def create_player_objects_from_log(game_log)
      match_players_nicks_from_log(game_log).map { |nick| Player.new(nick) }
    end

    def match_players_nicks_from_log(game_log)
      select_nicks_from_log(game_log).map { |line| player_match_expression.match(line)[:nick] }.uniq
    end

    def select_nicks_from_log(game_log)
      select_text_from_log(game_log, 'ClientUserinfoChanged:')
    end

    def player_match_expression
      /ClientUserinfoChanged:\s\d+\sn\\(?<nick>.+)\\t\\/
    end
  end
end
