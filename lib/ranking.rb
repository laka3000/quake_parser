class Ranking
  class << self
    def rank_players_from_games(games)
      @players = {}
      extract_players_from_games(games)
      rank_players
    end

    private

    def rank_players
      @players.sort_by(&:last).reverse.map { |position| "#{position.first}: #{position[1]}" }.join("\n")
    end

    def extract_players_from_games(games)
      games.each { |game| insert_players_into_hash(game.players) }
    end

    def insert_players_into_hash(players)
      players.each { |player| update_player_values(player) }
    end

    def update_player_values(player)
      @players.key?(:"#{player.nick}") ? sum_value(player) : set_value(player)
    end

    def sum_value(player)
      @players[:"#{player.nick}"] += player.kills
    end

    def set_value(player)
      @players[:"#{player.nick}"] = player.kills
    end
  end
end
