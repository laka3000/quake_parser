require 'text_selector'

class TotalKillParser
  extend TextSelector

  class << self
    def extract_total_kills_from_log(game_log)
      game_kills(game_log).length
    end

    private

    def game_kills(game_log)
      select_text_from_log(game_log, 'Kill:')
    end
  end
end
