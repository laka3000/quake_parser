require 'spec_helper'
require 'game_log_slicer'

describe GameLogSlicer do

  let(:path) { 'a_path' }

  describe '.slice_games_from_log' do
    it 'extracts each individual game log from the log file' do
      line_break = '-' * 60
      init_game = '0:00 InitGame:'
      log_text = [init_game, line_break, init_game, line_break, init_game, line_break]

      allow(described_class).to receive(:read_log_file_from_path).and_return(log_text)

      game_logs = described_class.slice_games_from_log(path)

      number_of_extracted_games = game_logs.length

      expect(number_of_extracted_games).to be(3)
    end
  end
end
