require 'spec_helper'
require 'quake_parser'

describe QuakeParser do

  describe '.parse' do
    let(:path) { './spec/test.log' }
    let(:total_kills_text) { '"total_kills": 3' }

    it 'parses a quake game log and prints it\'s games and a ranking onscreen' do

      expect(STDOUT).to receive(:puts).with(/#{total_kills_text}/)
      described_class.parse(path)
    end
  end
end
