require 'spec_helper'
require 'games_to_json_parser'

describe GamesToJsonParser do

  let(:games_dummy) { double('game') }

  describe '.parse_games_to_json' do
    it 'creates a JSON object from a set of games' do

      allow(described_class).to receive(:hash_of_all_games).and_return( { game_1: games_dummy } )

      returned_json = described_class.parse_games_to_json( [games_dummy] )

      expect(returned_json).to include('game_1')
    end
  end
end
