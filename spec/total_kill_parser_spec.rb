require 'spec_helper'
require 'total_kill_parser'

describe TotalKillParser do

  describe '#extract_total_kills_from_log' do

    it 'counts the total kills in a game' do
      kill_1_text = '20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT'
      kill_2_text = '20:54 Kill: 1 2 22: John killed Doe by MOD_ROCKET_SPLASH'
      kill_3_text = '20:12 Kill: 3 2 22: Jane killed Doe by MOD_ROCKET_SPLASH'

      log_text = [kill_1_text, kill_2_text, kill_3_text]

      kill_count = described_class.extract_total_kills_from_log(log_text)

      expect(kill_count).to be(3)
    end
  end
end
