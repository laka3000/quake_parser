require 'spec_helper'
require 'player'
require 'ranking'

describe Ranking do

  describe '.rank_players_from_games' do
    let(:player1) { double('Player 1').as_null_object }
    let(:player2) { double('Player 2').as_null_object }
    let(:game1) { double('Game') }
    let(:game2) { double('Game') }

    it 'rank all players from games' do
      allow(player1).to receive(:nick).and_return('Player 1')
      allow(player2).to receive(:nick).and_return('Player 2')

      allow(player1).to receive(:kills).and_return(20)
      allow(player2).to receive(:kills).and_return(1)

      allow(game1).to receive(:players).and_return([player1])
      allow(game2).to receive(:players).and_return([player2])

      games = [game1, game2]

      ranking = described_class.rank_players_from_games(games)

      expect(ranking).to include(player1.nick)
      expect(ranking).to include(player2.nick)
    end
  end
end
