require 'printer'

describe Printer do

  describe '.print' do
    let(:game_json) { double('Game_1') }
    let(:ranking) { double('Ranking') }

    it 'rank all players from games' do
      expect(STDOUT).to receive(:puts).with(/Player ranking:/)
      described_class.print(game_json, ranking)
    end
  end
end
