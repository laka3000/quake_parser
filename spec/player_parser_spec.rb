require 'spec_helper'
require 'player_parser'

describe PlayerParser do

  describe '.extract_players_from_log' do

    it 'creates players from a log text' do
      init_game = '0:00 InitGame:'
      player1_enters = '20:38 ClientUserinfoChanged: 1 n\Player 1\t\\'
      player2_enters = '20:38 ClientUserinfoChanged: 2 n\Player 2\t\\'
      player1_enters_again = player1_enters

      log_text = [init_game, player1_enters, player2_enters, player1_enters_again]

      players = described_class.extract_players_from_log(log_text)

      player_1_nick = players.first.nick

      number_of_players = players.length

      expect(player_1_nick).to eq('Player 1')
      expect(number_of_players).to be(2)
    end
  end
end
