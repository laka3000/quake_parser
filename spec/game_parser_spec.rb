require 'spec_helper'
require 'game_parser'

describe GameParser do

  describe '.extract_game_from_log_slice' do
    let(:players) { [double('Player 1'), double('Player 2')] }
    let(:log) { double('log') }

    it 'creates a game from a log text' do
      allow(described_class).to receive(:total_kills).and_return(10)
      allow(described_class).to receive(:players).and_return(players)

      generated_game = described_class.extract_game_from_log_slice(log)

      expect(generated_game.class).to be(Game)

      expect(generated_game.total_kills).to eq(10)
    end
  end
end
