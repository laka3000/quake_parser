describe TextSelector do

  describe '#select_text_from_log' do
    let(:test_class) { Class.new { extend TextSelector } }

    it 'returns an array with the lines which contains a certain text from a log' do

      correct_text = 'This is the text it should return'
      incorrect_text = 'This is definitely not the text it should return'
      also_incorrect_test = 'This also should not be the text it retured'

      log_text = [incorrect_text, correct_text, also_incorrect_test]

      returned_text = test_class.select_text_from_log(log_text, correct_text).first

      expect(returned_text).to be(correct_text)
    end
  end
end
