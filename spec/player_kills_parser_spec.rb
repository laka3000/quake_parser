require 'spec_helper'
require 'player_kills_parser'

describe PlayerKillsParser do

  describe '.update_player_kills_from_log' do

    let(:player) { Player.new('Player') }

    it 'sets the number of kills for each player' do
      player_kills = 'Kill: 3 2 6: Player killed Player2 by MOD_ROCKET'
      player_dies_to_world = '20:54 Kill: 1022 2 22: <world> killed Player by MOD_TRIGGER_HURT'

      log_text = [player_dies_to_world, player_dies_to_world, player_kills]

      described_class.update_player_kills_from_log(log_text, [player])

      player_kills = player.kills

      expect(player_kills).to eq(-1)
    end
  end
end
